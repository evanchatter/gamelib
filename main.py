from classes import *
from time import sleep

# Create board

print("\033[1m| Welcome to Settlers of Catan! |\033[0m\nChoose your board type: ")
choice = getChoice(["starter", "variable", "random"])
catanBoard = Board(choice)
catanDeck = Deck()

# Create players

print("How many players?")
choice = getChoice([n for n in range(1, 5)])
players = [0 for x in range(choice)]

# Colour assignment
opts = {"red", "blue", "yellow", "green"}
for i in range(choice):
    name = input(f"Player {i + 1}, choose a name: ")

    print(f"{name}, choose a colour: ")
    colour = getChoice(opts)
    opts.discard(colour)

    players[i] = Player(colour, name)

print("The game has begun.\nStarting board is: ")
catanBoard.printBoard()

# Place initial settlements/roads

print("\n\033[1m| Initial Settlement Assignment Phase |\033[0m\n")
if catanBoard._genMethod == "starter":
    sLocs = {
        "red" : ((6, 1), (2, 1)),
        "blue" : ((8, 1), (8, 3)),
        "yellow" : ((0, 1), (0, 1)),
        "green" : ((5, 1), (6, 4))
    }
    for p in players:
        placeSettlement(catanBoard, p, True, sLocs[p.colour][0])
        placeRoad(catanBoard, p, p.settles[0])

    catanBoard.produce(True)
    for p in players:
        p.printResources()

    for p in players:
        placeSettlement(catanBoard, p, True, sLocs[p.colour][1])
        placeRoad(catanBoard, p, p.settles[1])

else:
    print("| Initial Settlement Placement |")
    for p in players:
        print(f"{p}, your setup phase has started.")

        placeSettlement(catanBoard, p, True)
        placeRoad(catanBoard, p, p.settles[0])

    # Loading anim

    out = "\n"
    for i in range(4):
        print("Adding initial resources" + (i * ".") + "\r", end = "")
        sleep(0.5)
    print(out)

    # First wave of resource production from first settlement

    catanBoard.produce(catanBoard.empty, True)
    for p in players:
        p.printResources()
        sleep(0.5)
    sleep(0.7)
    print("\n")

    for p in players:
        placeSettlement(catanBoard, p, True)
        placeRoad(catanBoard, p, p.settles[1])

# Primary game loop
playing = True
winner = None
while playing:
    for p in players:
        print(f"It's {p}'s turn.")

        #1. Roll for resources

        out = "\n"
        for i in range(4):
            print("Rolling for production" + (i * ".") + "\r", end = "")
            sleep(0.5)
        print(out)

        catanBoard.produce(p)
        p.printResources()
    
        #2. Menu for Trading, Building, Card Buying/Playing

        inTurn = True
        while inTurn:
            print("\n| Turn Menu |\nSelect an action:")
            action = getChoice(["trade", "buy", "play", "pass"], ["Trade (with someone)", "Buy (something)", "Play (a card)", "End (turn)"])

            #2a. Trading

            if action == "trade":
                action2 = getChoice(["port", "player"], ["Port (trading)", "Player (trading)"])

                if action2 == "port":
                    #Port trading              
                    print("Using:")
                    p.portTrade()

                else:
                    print("Players' resources are:")
                    for q in players:
                        if player != q: q.printResources()

                    print("Pick a player.")
                    choice = getChoice([p.name for p in players])
                    for q in players:
                        if choice == q.name:
                            tar = q
                    p.offerTrade(tar)

            #2b. Building/Card Buying

            if action == "buy":
                vBuys, vDisps = [], []
                if ((p.resources["Wood"] >= 1) & (p.resources["Brick"] >= 1)):
                    vBuys.append("road")
                    vDisps.append("Road (placement)")
                if (p.resources["Wood"] >= 1) & (p.resources["Brick"] >= 1) & (p.resources["Sheep"] >= 1) & (p.resources["Wheat"] >= 1): 
                    vBuys.append("settlement")
                    vDisps.append("Settlement (placement)")
                if (p.resources["Wheat"] >= 2) & (p.resources["Ore"] >= 3):
                    vBuys.append("city")
                    vDisps.append("City (upgrade)")
                if (p.resources["Sheep"] >= 1) & (p.resources["Wheat"] >= 1) & (p.resources["Ore"] >= 1) & (catanDeck.cards != []): 
                    vBuys.append("card")
                    vDisps.append("Development Card (purchase)")

                action2 = getChoice(vBuys, vDisps)

                if action2 == "road":
                    placeRoad(catanBoard, p)
                    p.resources["Wood"] -= 1
                    p.resources["Brick"] -= 1
                elif action2 == "settlement":
                    placeSettlement(catanBoard, p)
                    p.resources["Wood"] -= 1
                    p.resources["Brick"] -= 1
                    p.resources["Sheep"] -= 1
                    p.resources["Wheat"] -= 1
                elif action2 == "city":
                    placeCity(catanBoard, p)
                    p.resources["Wheat"] -= 2
                    p.resources["Ore"] -= 3
                else:
                    player.cards.append(deck.draw(p))
                    if card == Victory:
                        p.points += 1
                    print(p.cards)
                    p.resources["Sheep"] -= 1
                    p.resources["Wheat"] -= 1
                    p.resources["Ore"] -= 1
                    


            #2c. Card Playing
            
            if action == "play":
                playable = []
                for c in p.cards:
                    if type(card) != Victory:
                        playable.append(c)
                getName = getChoice([c.name.lower() for c in playable], [d.name for d in playable])
                getName.upper()
                for i, c in enumerate(p.cards):
                    if c.name == getName:
                        toPlay = c
                        p.cards.pop(i)
                        break
                toPlay.play()
                if type(card) == Knight:
                    p.knights += 1

            else:
                inTurn = False

        #3. Score check
        if (p.knights >= 3) & (p.knights > largestArmy.knights):
            print(f"{p} has claimed the largest army from {largestArmy}!")
            largestArmy.points -= 2
            largestArmy = p
            largestArmy.points += 2
        if (p.roadLength >= 5) & (p.roadLength > longestRoad.roadLength):
            print(f"{p} has claimed the longest road from {longestRoad}!")
            longestRoad.points -= 2
            longestRoad = p
            longestRoad.points += 2
        if p.points >= 10:
            winner = p
            playing = False

# Game End

print("| The Game is Over. |")
sleep(0.5)
print(f"{winner} has won!")
sleep(0.5)
print("Scores are:")
for p in players:
    print(f"{p} ({p.score})\n")
