import sys
from random import choice, choices, shuffle, randint
import time
import re
import math

# ANSI codes from input colour; propend to strings
def ansi(colour):
    codeDict = {
        "red" : "0;31m",
        "blue" : "0;34m",
        "yellow" : "1;33m",
        "green" : "0;32m"
    }
    if colour in codeDict: return "\033[" + codeDict[colour]
    else: return ""

## Player Class; holds information on player, serves as owned tag for vertices
class Player:
    def __init__(self, colour, name = "Empty"):
        self.resources = {}
        for s in ["Brick", "Wood", "Ore", "Wheat", "Sheep"]:
            self.resources[s] = 0
        self.cards, self.settles = [], []
        self.trades = set()
        self._colour, self._name = colour, name
        self.networks = []
        self.knights, self.roadLength, self.score = 0, 0, 0

    def __repr__(self):
        return f"{ansi(self.colour)}{self.name}\033[0m"
    
    @property
    def name(self):
        return self._name
    
    @property
    def colour(self):
        return self._colour

    def printResources(self):
        resDict = {
            "Brick" : "\033[0;35mBrick: \033[0m",
            "Wood" : "\033[0;32mWood: \033[0m",
            "Ore" : "\033[1;30mOre: \033[0m",
            "Wheat" : "\033[0;33mWheat: \033[0m",
            "Sheep" : "\033[0;34mSheep: \033[0m",
        }
        out = ("\n" + "\033[1m" + self.name + "\033[0m" + "\n")
        for r, n in self.resources.items():
            out += (resDict[r] + str(n) + " | ")
        print(out)
    
    def portTrade(self):
        validTrades = set()
        for prt in validTrades:
            if trade == "All":
                canSpend = ()
                for rs in self.resources:
                    if self.resources[rs] >= 3:
                        validTrades.add("any")
                        canSpend.append(rs.lower())
            else:
                if self.resources[trade.capitalize()] >= 2:
                    validTrades.add(trade.lower())
        if validTrades == set():
            print("No valid trades. Returning to menu.")
        else:
            choice = getChoice(validTrades, [t.capitalize() for t in validTrades])
            if choice == "any":
                print("Choose a resource to spend:")
                given = getChoice(canSpend, [r.capitalize() for r in canSpend])
                print("Choose a number of times to trade:")
                spent = getChoice([n for n in range(1, floor(self.resources[spent.capitalize()]) / 3) + 1], f"Between 1 and {floor(self.resources[spent.capitalize()]) / 3}")
                self.resources[given] -= (spent * 3)
            else:
                print(f"Spending {choice}.")
                print(f"Choose a number of times to trade:")
                spent = getChoice([n for n in range(1, floor(self.resources[spent.capitalize()]) / 2) + 1], f"Between 1 and {floor(self.resources[spent.capitalize()]) / 2}")
                self.resources[given] -= (spent * 2)
            for n in range(1, given + 1):
                print(f"Choose the resource to recieve for Trade {n}:")
                gotten = getChoice([res for res in self.resources], [vres.capitalize() for vres in self.resources])
                self.resources[gotten] += 1
                
    def offerTrade(self, tar):
        disps = ["\033[0;35mBrick\033[0m", "\033[0;32mWood\033[0m", "\033[1;30mOre\033[0m", "\033[0;33mWheat\033[0m", "\033[0;34mSheep\033[0m"]
        chxs = ["brick", "wood", "ore", "wheat", "sheep"]
        dispArr, chxArr = [], []
        for i, c in enumerate(chxs):
            if tar.resources[c.capitalize()] > 0:
                chxArr.append(chxs[i])
                dispArr.append(disps[i])
        if all(v == 0 for v in self.resources) | all(v == 0 for v in tar.resources):
            tar.printResources()
            self.printResources()

            print("Choose a resource:")
            resTake = getChoice(chxArr, dispArr)

            print("Choose an amount:")
            qtyTake = getChoice(range(1, tar.resources(resTake.capitalize()) + 1), f"between 1 and {tar.resources(resTake.capitalize()) + 1}")
            
            dispArr, chxArr = [], []
            for i, c in enumerate(chxs):
                if tar.resources[c.capitalize()] > 0:
                    chxArr.append(chxs[i])
                    dispArr.append(disps[i])

            print("Choose a resource:")
            resGive = getChoice(chxArr, dispArr)

            print("Choose an amount:")
            qtyGive = getChoice(range(1, self.resources(resGive.capitalize()) + 1), f"between 1 and {self.resources(resGive.capitalize()) + 1}")

            # Execute trade
            print(f"{tar.name}, do you accept the trade?")
            conf = getChoice(["yes", "no"], ["Yes", "No"])
            if conf == "yes":
                self.resources[resGive] -= qtyGive
                tar.resources[resGive] += qtyGive
                tar.resources[resTake] -= qtyGive
                self.resources[qtyTake] += qtyGive
            else:
                print("Trade rejected. Returning to menu.")
        else:
            print("One or both of the players has no resources. Returning to menu.")

### Catan Gameboard; graph of deg. 2/3 nodes which make hexes @ every cycle of size 6. Hexes assigned to board with val/res.
## Vertex Nodes
class Vertex:
    #Roads are stored as pairs of [target vertex, owner]
    def __init__(self, owner, roads = None, port = "None", isCity = False):
        if roads == None: roads = []
        self._roads = roads
        self.owner = owner
        self._port = port
        self.isCity = isCity
        self.seen = False
    def __repr__(self):
        return f"V"
    @property
    def roads(self):
        return self._roads
    @property
    def port(self):
        return self._port
    def roadColour(self, tar):
        for i, r in enumerate(self.roads):
            if tar == r[0]:
                for j, s in enumerate(tar.roads):
                    if self == s[0]:
                        return self.roads[i][1].colour
        raise ValueError("Target road does not exist.")
    def updateRoad(self, tar, player):
        for i, r in enumerate(self.roads):
            if tar == r[0]:
                for j, s in enumerate(tar.roads):
                    if self == s[0]:
                        self._roads[i][1] = player
                        tar._roads[j][1] = player
                        return
        raise ValueError("Target road does not exist.")
    def playerRoads(self, player):
        pRoads = []
        for r in self.roads:
            if r[1] == player:
                pRoads.append(r[0])
        return pRoads

## Hex Tiles
class Tile:
    def __init__(self, vertices, resource = "None", value = 0, isRobbed = False):
        self._resource = resource
        self._vertices = vertices
        self._value = value
        self.isRobbed = isRobbed
    def __repr__(self):
        return f"T"
    @property
    def resource(self):
        return self._resource
    @property
    def vertices(self):
        return self._vertices
    @property
    def value(self):
        return self._value
    def terrain(self):
        resDict = {
            "Brick" : "\033[0;35m  Hills  \033[0m",
            "Wood" : "\033[0;32m Forests \033[0m",
            "Ore" : "\033[1;30mMountains\033[0m",
            "Wheat" : "\033[0;33m  Field  \033[0m",
            "Sheep" : "\033[0;34m Pasture \033[0m",
            "None" : "\033[0;31m Deserts \033[0m"
        }
        if self.resource in resDict: return resDict[self.resource]
        else: return ""
## Final Board Construction
class Board:
    def __init__(self, genMethod):
        self._genMethod = genMethod
        self._tileset = [[0 for c in range(5)] for r in range(5)]
        self.empty = Player("none")
        self.longestRoad, self.largestArmy = self.empty, self.empty
        ## Build Vertex Graph
        g1, g2 = [[0 for c in range(6)] for r in range(6)], [[0 for c in range(6)] for r in range(6)]
        for g in [g1, g2]:
            for r in range(6):
                if not r % 2:
                    for c in range(3 + (r // 2)):
                        g[r][c] = Vertex(self.empty)
                        if not ((r == 0) | (g[r - 1][c] == 0)):
                            g[r][c]._roads.append([g[r - 1][c], self.empty])
                            g[r - 1][c]._roads.append([g[r][c], self.empty])
                else:
                    for c in range(3 + (-(r // -2))):
                        g[r][c] = Vertex(self.empty)
                        if not ((c == 0 | r == 0) | (g[r - 1][c - 1] == 0)):
                            g[r][c]._roads.append([g[r - 1][c - 1], self.empty])
                            g[r - 1][c - 1]._roads.append([g[r][c], self.empty])
                        if not ((r == 0) | (g[r - 1][c] == 0)):
                            g[r][c]._roads.append([g[r - 1][c], self.empty]) 
                            g[r - 1][c]._roads.append([g[r][c], self.empty])

        self._graph = g1 + g2[::-1]
        for c in range(6):
            if not [self._graph[6][c], self.empty] in self._graph[5][c]._roads: self._graph[5][c]._roads.append([self._graph[6][c], self.empty])
            if not [self._graph[5][c], self.empty] in self._graph[6][c]._roads: self._graph[6][c]._roads.append([self._graph[5][c], self.empty])
        # Assign to Hexes
        hexVerts = []
        for r in range(1, 10, 2):
            vLen = 0
            for i in self._graph[r]:
                if type(i) == Vertex:
                    vLen += 1
            for c in range(0, vLen - 1):
                hexVerts.append((self._graph[r][c],
                self._graph[r - 1][c] if r <= 6 else self._graph[r - 1][c + 1],
                self._graph[r][c + 1],
                self._graph[r + 1][c + 1],
                self._graph[r + 2][c + 1] if r <= 4 else self._graph[r + 2][c],
                self._graph[r + 1][c]))
        # Starter board (manual)
        if self._genMethod == "starter":
            self._tileset[0][2] = Tile(hexVerts[0], "Ore", 10)
            self._tileset[0][3] = Tile(hexVerts[1], "Sheep", 2)
            self._tileset[0][4] = Tile(hexVerts[2], "Wood", 9)
            self._tileset[1][1] = Tile(hexVerts[3], "Wheat", 12)
            self._tileset[1][2] = Tile(hexVerts[4], "Brick", 6)
            self._tileset[1][3] = Tile(hexVerts[5], "Sheep", 4)
            self._tileset[1][4] = Tile(hexVerts[6], "Brick", 10)
            self._tileset[2][0] = Tile(hexVerts[7], "Wheat", 9)
            self._tileset[2][1] = Tile(hexVerts[8], "Wood", 11)
            self._tileset[2][2] = Tile(hexVerts[9], "None", 0, True)
            self._tileset[2][3] = Tile(hexVerts[10], "Wood", 3)
            self._tileset[2][4] = Tile(hexVerts[11], "Ore", 8)
            self._tileset[3][0] = Tile(hexVerts[12], "Wood", 8)
            self._tileset[3][1] = Tile(hexVerts[13], "Ore", 3)
            self._tileset[3][2] = Tile(hexVerts[14], "Wheat", 4)
            self._tileset[3][3] = Tile(hexVerts[15], "Sheep", 5)
            self._tileset[4][0] = Tile(hexVerts[16], "Brick", 5)
            self._tileset[4][1] = Tile(hexVerts[17], "Wheat", 6)
            self._tileset[4][2] = Tile(hexVerts[18], "Sheep", 11)
            self._tileset[0][2]._vertices[0]._port, self._tileset[0][2]._vertices[1]._port = "All", "All"
            self._tileset[2][4]._vertices[2]._port, self._tileset[2][4]._vertices[3]._port = "All", "All"
            self._tileset[4][1]._vertices[3]._port, self._tileset[4][1]._vertices[4]._port = "All", "All"
            self._tileset[4][0]._vertices[4]._port, self._tileset[4][0]._vertices[5]._port = "All", "All"
            self._tileset[0][3]._vertices[1]._port, self._tileset[0][3]._vertices[2]._port = "Wheat", "Wheat"
            self._tileset[1][1]._vertices[0]._port, self._tileset[1][1]._vertices[5]._port = "Wood", "Wood"
            self._tileset[1][4]._vertices[1]._port, self._tileset[1][4]._vertices[2]._port = "Ore", "Ore"
            self._tileset[3][0]._vertices[1]._port, self._tileset[3][0]._vertices[5]._port = "Brick", "Brick"
            self._tileset[3][3]._vertices[3]._port, self._tileset[3][3]._vertices[4]._port = "Sheep", "Sheep"
        else:
            resArr = ["Brick", "Brick", "Brick", "Wood", "Wood", "Wood", "Wood", "Ore", "Ore", "Ore", 
            "Wheat", "Wheat", "Wheat", "Wheat", "Sheep", "Sheep", "Sheep", "Sheep", "None"]
            shuffle(resArr)
            prtArr = ["All", "All", "All", "All", "Wheat", "Wood", "Ore", "Brick", "Sheep"]
            shuffle(prtArr)
            prtLocs = [((2, 0), (0, 1)), ((2, 1), (1, 2)), (3, (5, 0)), (6, (1, 2)), 
            (11, (2, 3)), (12, (5, 0)), (15, (3, 4)), (16, (4, 5)), (17, (3, 4))]
            # Middle is (2, 2) -> (p, q); top left is (2, 0) -> (p, q)
            i, j = 0, 0
            for q in range(5):
                for p in range(max(2 - q, 0), min(7 - q, 5)):
                    self._tileset[q][p] = Tile(hexVerts[i], resArr.pop())
                    if self._tileset[q][p].resource == "None":
                        self._tileset[q][p].isRobbed = True
                    if (p, q) in prtLocs[j][0]:
                        prt = str(prtArr.pop())
                        self._tileset[p][q]._vertices[prtLocs[j][1][0]]._port = prt
                        self._tileset[p][q]._vertices[prtLocs[j][1][1]]._port = prt 
                        j += 1
                    i += 1
            if self._genMethod == "variable":
                valArr = [11, 3, 6, 5, 4, 9, 10, 8, 4, 11, 12, 9, 10, 8, 3, 6, 2, 5]
                spiral = [(2, 0), (1, 1), (0, 2), (0, 3), (0, 4), (1, 4), (2, 4), 
                          (3, 3), (4, 2), (4, 1), (4, 0), (3, 0), (2, 1), (1, 2),
                          (1, 3), (2, 3), (3, 2), (3, 1), (2, 2)]
                for i in range(19):
                    #Inconsistent indexing from the rest becuz im lazy, sry
                    if self._tileset[spiral[i][1]][spiral[i][0]].resource != "None":
                        self._tileset[spiral[i][1]][spiral[i][0]]._value = valArr.pop()
            else:
                validBoard = False
                while not validBoard:
                    nValArr = [2, 3, 3, 4, 4, 5, 5, 9, 9, 10, 10, 11, 11, 12]
                    shuffle(nValArr)
                    hValArr = [6, 6, 8, 8]
                    shuffle(hValArr)
                    for q in range(5):
                        for p in range(max(2 - q, 0), min(7 - q, 5)):
                            if self._tileset[p][q].resource != "None":
                                if hValArr == []:
                                    self._tileset[p][q]._value = nValArr.pop()
                                elif nValArr == []:
                                    self._tileset[p][q]._value = hValArr.pop()
                                else:
                                    self._tileset[p][q]._value = choices(population = [nValArr.pop(), hValArr.pop()], weights = [0.78, 0.22], k = 1)
                    for q in range(5):
                        for p in range(max(2 - q, 0), min(7 - q, 0)):
                            if self._tileset[p][q].value in (6, 8):
                                validTiles = 0
                                adjValues, a = [], 0
                                for d in [(0, -1), (1, -1), (-1, 0), (1, 0), (-1, 1), (0, 1)]:
                                    try:
                                        a = self._tileset[p + d[0]][q + d[1]].value
                                    except IndexError():
                                        a = Tile()
                                    finally:
                                        if a != Tile():
                                            adjValues.append(a)
                                if any(v in (6, 8) for v in adjValues):
                                    validTiles += 1
                    if validTiles == 19:
                        validBoard = True

    @property
    def graph(self):
        return self._graph

    @property
    def tileset(self):
        return self._tileset
    
    @property
    def genMenthod(self):
        return self._genMethod

    # Returns adjacent tiles to target tile, with any invalid positions subbed for -1. 

    def adjTiles(self, tile):
        q, p = -1, -1
        for i, r in enumerate(self.tileset):
            for j, t in enumerate(r):
                if type(t) == Tile:
                    if tile == t:
                        q, p = i, j
        if((q == -1) & (p == -1)):
            raise ValueError("Tile not in dataset")
        else:
            diffs = [(-1, 0), (-1, 1), (0, 1), (1, 0), (1, -1), (0, -1)]
            adjs = []
            for d in diffs:
                try:
                    a = self.tileset[q + d[0]][p + d[1]]
                except IndexError:
                    a = -1
                adjs.append(a)
            return adjs

    def printBoard(self, sel = None):
        #no tabs so that concatenation stops breaking on inserted characters
        vertArr, slshArr, tileArr = [], [], []
        tilesetFlat = flattenTiles(self.tileset)
        t = 0

        for i, ru in enumerate(self.graph):
            r = [j for j in ru if type(j) == Vertex]
            vertLine = "            "
            vertLine += ''.join(["        " for _ in range(6 - len(r))])
            for v in r: 
                if v == sel:
                    vertLine += (f"{ansi(v.owner.colour)}" + f"\u25fc" + "\033[0m" + "               ")
                else:
                    icon = "\u25fb" if v.owner == self.empty else ("\u26eb" if v.isCity else "\u2302")
                    vertLine += (f"{ansi(v.owner.colour)}" + f"{icon}" + "\033[0m" + "               ")

            vertArr.append(vertLine)

            # for vertex -> tile conversion when printing selected; pretty inelegant solution tbh
            
            if i != 0:
                slshLine = "            "
                if i % 2:
                    slshLine += (''.join(["        " for _ in range(6 - len(r))]))
                    if i <= 5: 
                        slshLine += "    "
                    else:
                        slshLine = slshLine[:-4]
                    for j, v in enumerate(r):
                        if i > 6:
                            slshLine += (f"{ansi(v.roadColour(self.graph[i - 1][j]))}" + "\\" + "\033[0m" + "       ")
                        elif v != r[0]:
                            slshLine += (f"{ansi(v.roadColour(self.graph[i - 1][j - 1]))}" + "\\" + "\033[0m" + "       ")
                        if i > 6:
                            slshLine += (f"{ansi(v.roadColour(self.graph[i - 1][j + 1]))}" + "/" + "\033[0m" + "       ")
                        elif v != r[-1]:
                            slshLine += (f"{ansi(v.roadColour(self.graph[i - 1][j]))}" + "/" + "\033[0m" + "       ")
                
                else:
                    slshLine += ''.join(["        " for _ in range(6 - len(r))])
                    for j, v in enumerate(r):
                        if (tilesetFlat[t] == sel) & (tilesetFlat[t].isRobbed) & (j != (len(r) - 1)): 
                            slshLine += (f"{ansi(v.roadColour(self.graph[i - 1][j]))}" + "|" + "\033[0m" + "       " + "\U0001F141" + "       ")
                        elif (tilesetFlat[t] == sel) & (j != (len(r) - 1)): 
                            slshLine += (f"{ansi(v.roadColour(self.graph[i - 1][j]))}" + "|" + "\033[0m" + "       " + "\u25fc" + "       ")
                        elif tilesetFlat[t].isRobbed & (j != (len(r) - 1)): 
                            slshLine += (f"{ansi(v.roadColour(self.graph[i - 1][j]))}" + "|" + "\033[0m" + "       " + "R" + "       ")
                        else:
                            slshLine += (f"{ansi(v.roadColour(self.graph[i - 1][j]))}" + "|" + "\033[0m" + "               ")
                        if j != (len(r) - 2): t += 1
                slshArr.append(slshLine)
                
        for i, ru in enumerate(self.tileset):
            r = [j for j in ru if type(j) == Tile]
            tileLine = "                    "
            tileLine += ''.join(["        " for _ in range(5 - len(r))])
            for t in r:
                val = str(t.value)
                if val == "0":
                    val = " "
                if len(val) <= 2:
                    val += ''.join([" " for _ in range(2, len(val), -1)])
                if ((val == "6 ") | (val == "8 ")):
                    val = "\033[0;31m" + val + "\033[0m"
                tileLine += (val + "              ")
            tileArr.append(tileLine)
            tileLine = "                    "
            tileLine += ''.join(["        " for _ in range(5 - len(r))])
            tileLine = tileLine[:-4]
            for t in r:
                tileLine += (f"{t.terrain()}" + "       ")
            tileArr.append(tileLine)
             
        for i in range(33):
            if (i % 6 == 0) | (i % 6 == 2):
                print(vertArr.pop(0))
            elif (i % 6 == 1) | (i % 6 == 4):
                print(slshArr.pop(0))
            else:
                print(tileArr.pop(0))

    # Resource at turn end function; adds resources on diceroll
    def produce(self, player, init = False):
        tilesetFlat = flattenTiles(self.tileset)
        roll = randint(1, 6) + randint(1, 6)
        if not init:
            print(f"Rolled a {roll}.")
        if (roll != 7) | (init):
            for t in tilesetFlat:
                if ((t.value == roll) | (init)) & (t.resource != "None") & (not t.isRobbed):
                    for v in t.vertices:
                        if (v.owner != self.empty):
                            for r in v.owner.resources:
                                if r == t.resource:
                                    v.owner.resources[r] += 1
                                    if v.isCity: v.owner.resources[r] += 1
        else:
            print(f"Rolled a 7. {player} must now move the robber.")
            self.doTheft(player)
    # Steal for for knights/7
    def doTheft(self, player):
        valid = False
        while not valid:
            tar = getLocation(self, "tile")
            if tar.isRobbed == True:
                print("Same tile. Please try again.")
            else:
                for org in flattenTiles(self.tileset):
                    if org.isRobbed:
                        org.isRobbed == False

                tar.isRobbed == True

                victims, victDict = set(), {}
                for v in tar.vertices:
                    if((not(all(a == 0 for a in v.owner.resources.values()))) & (v.owner != player)):
                        victims.add(v.owner)
                # Empty case should be handled by 0 case; leaving in as contingency
                victims.discard(self.empty)
                # Player object list to backreference from name choice
                #for v in victims:
                    #clearName = v.compile(r"/(\x9B|\x1B\[)[0-?]*[ -\/]*[@-~]/")
                    #for p in prms:
                        #victDict[(clearName.sub('', p)).lower()] = [v]
                if victims == set():
                    print("No targets for theft here.")
                else:
                    print("Steal from?")
                    choice = getChoice([v.name for v in victims])
                    for v in victims:
                        if choice == v.name.lower():
                            steal = v
                    card = choices(population = [r for r in steal.resources.keys()], weights = [v for v in steal.resources.values()], k = 1)
                    steal.resources[card[0]] -= 1
                    player.resources[card[0]] += 1
                    print(f"Stole a {card[0]}.")
                valid = True
    # Graph traversal to longest road; call minimally, this thing is weirdly expensive
    def getLongestRoad(self, player):
        maxLength = 0

        for r in self.graph:
            for v in r:
                if type(v) == Vertex:
                    if ((v.seen == False) & (len(v.playerRoads(player)) == 1)):
                        routes = [[v]]
                        v.seen = True
                        while routes != []:
                            sourceRoute = routes.pop()
                            destRoutes = []
                            for sv in sourceRoute:
                                for dv in sv.playerRoads(player):
                                    if dv.seen == False:
                                        destRoutes.append(sourceRoute + [dv])
                                        dv.seen = True
                        if destRoutes == []:
                            maxLength = max(maxLength, len(sourceRoute))
                        else:
                            for dr in destRoutes:
                                routes.append(dr)
                    else:
                        v.seen = True

        for r in self.graph:
            for v in r:
                if type(v) == Vertex:
                    if ((v.owner == self.empty) | (v.owner == player)):
                        v.seen = False
                    else:
                        v.seen = True
        player.roadLength = maxLength

        
                
                
#(Should definitely have made a road class for posterity)

##Development Cards
class DevCard:
    def __init__(self, name):
        self._name = name
    @property
    def name(self):
        return self._name
    def __str__(self):
        return f"{self._name}"
    def play(self):
        raise Exception("Not playable")
        return
        
#Children for card types
class Victory(DevCard):
    def __init__(self, name, points = 1):   
        super().__init__(name)
        self.points = points
    def __str__(self):
        return f"{self._name} (Victory)"

class Knight(DevCard):
    def __init__(self, name):
        super().__init__(name)
    def __str__(self):
        return f"{self._name} (Knight)"
    def play(self, board, player):
        board.doTheft(player)

class Progress(DevCard):
    def __init__(self, name):
        super().__init__(name)
    def __str__(self):
        return f"{self._name} (Progress)"
    #update later
    def play(self, name, board, player, targetList):
        if name == "Road Building":
            placeRoad(board, player)
            placeRoad(board, player)
        elif name == "Year of Plenty":
            print("Pick 2 resources to gain.")
            res = getChoice([r.lower() for r in player.resources], [r for r in player.resources])
            player.resources[res.upper()] += 1
            res = getChoice([r.lower() for r in player.resources], [r for r in player.resources])
            player.resources[res.upper()] += 1
        elif name == "Monopoly":
            print("Pick a resource to steal 1 of from all other players.")
            res = getChoice([r.lower() for r in player.resources], [r for r in player.resources])
            res.upper()
            stolen = 0
            for t in targetList:
                if t.resources[res] > 0: 
                    t.resources[res] -= 1
                    stolen += 1
            player.resources[res] += stolen

# Deck class
class Deck:
    def __init__(self):
        self.cards = []
        for name in ["Library", "Market", "Chapel", "Great Hall", "University"]:
            self.cards.append(Victory(name))
        for name in ["Army" * 14]:
            self.cards.append(Knight(name))
        for name in ["Year of Plenty", "Road Building", "Monopoly"]:
            self.cards.append(Progress(name))
            self.cards.append(Progress(name))
        shuffle(self.cards)

    def draw(self, player):
        drawn = self.cards.pop()
        if type(drawn) == Victory:
            player.points += 1
        return drawn

## User flow functions

# Flatten tileset array
def flattenTiles(tileset):
    flat = []
    for r in tileset:
        for t in r:
            if type(t) == Tile:
                flat.append(t)
    return flat

# Choice getter w/ exception handling
def getChoice(prms, disp = ""):
    # For parsing out ansi codes in params
    parsedPrms = []
    escaped = re.compile(r"/(\x9B|\x1B\[)[0-?]*[ -\/]*[@-~]/")
    for p in prms:
        if type(p) == str:
            parsedPrms.append((escaped.sub('', p)).lower())
        else:
            parsedPrms.append(p)

    if disp == "":
        disp = parsedPrms

    valid = False
    while not valid:
        
        choice = input(f"\nChoose one of {disp}: ")

        # Correct remainder
        if choice.isalpha():
            choice = choice.lower()
        if choice.isnumeric():
            choice = int(choice)
        if choice in parsedPrms:
            valid = True
            return choice

        print(f"Invalid choice. Please select from: {disp}")

# Converts relative location of origin vertex -> list of pairs of (vertex, arrow) for print
def toArrows(board, origin):
    chars = []
    for q, r in enumerate(board.graph):
        for p, v in enumerate(r):
            if origin == v:
                coords = [p, q]
    if coords[1] < 6:
        if coords[1] % 2:
            for c in [(coords[0] - 1, coords[1] - 1, "\u2196"), (coords[0], coords[1] - 1, "\u2197"), (coords[0], coords[1] + 1, "\u2193")]:
                try:
                    vert = board.graph[c[1]][c[0]]
                except IndexError:
                    vert = ""
                finally:
                    if(vert != ""):
                        chars.append([vert, c[2]])
        else:
            for c in [(coords[0], coords[1] - 1, "\u2191"), (coords[0], coords[1] + 1, "\u2199"), (coords[0] + 1, coords[1] + 1, "\u2198")]:
                try:
                    vert = board.graph[c[1]][c[0]]
                except IndexError:
                    vert = ""
                finally:
                    if(vert != ""):
                        chars.append([vert, c[2]])
    else:
        if coords[1] % 2:
            for c in [(coords[0], coords[1] - 1, "\u2196"), (coords[0] + 1, coords[1] - 1, "\u2197"), (coords[0], coords[1] + 1, "\u2193")]:
                try:
                    vert = board.graph[c[1]][c[0]]
                except IndexError:
                    vert = ""
                finally:
                    if(vert != ""):
                        chars.append([vert, c[2]])
        else:   
            for c in [(coords[0], coords[1] - 1, "\u2191"), (coords[0] - 1, coords[0] + 1, "\u2199"), (coords[0], coords[1] + 1, "\u2198")]:
                try:
                    vert = board.graph[c[1]][c[0]]
                except IndexError:
                    vert = ""
                finally:
                    if(vert != ""):
                        chars.append([vert, c[2]])
    return chars

# Returns location (either vertex or tile based on get)
def getLocation(board, get):

    if get == "tile":
        selTile, finTile = board.tileset[2][2], False
        while not finTile:
            board.printBoard(selTile)

            print(f"Move with 1: \u2196, 2: \u2197, 3: \u2192, 4: \u2198, 5: \u2199, 6: \u2190. Select with 0.")

            m = input("::")
            if m.isnumeric():
                m = int(m)
                if(m == 0):
                    finTile = True
                elif(m < 7):
                    if type(board.adjTiles(selTile)[m - 1]) == Tile:
                        selTile = board.adjTiles(selTile)[m - 1]
                    else:
                        print("Out of bounds. Try again.")
                else:
                    print("Invalid choice. Try again.")
            else:
                print("Invalid choice. Try again.")
        return selTile

    elif get == "vertex":
        selVert, finVert = board.tileset[2][2].vertices[0], False
        while not finVert:
            board.printBoard(selVert)
            chars = toArrows(board, selVert)
            if len(selVert.roads) == 2:
                print(f"Move with 1: {chars[0][1]}, 2: {chars[1][1]}. Select with 0.")
            else:
                print(f"Move with 1: {chars[0][1]}, 2: {chars[1][1]}, 3: {chars[2][1]}. Select with 0.")

            m = input("::")
            if m.isnumeric():
                m = int(m)
                if(m == 0): 
                    finVert = True
                elif(((len(selVert.roads) == 2) & (m <= 2)) | ((len(selVert.roads) == 3) & (m <= 3))):
                    if chars[m - 1][0] != 0: selVert = chars[m - 1][0] 
                    else: print("Out of bounds. Try again.")
                else:
                    print("Invalid choice. Try again.")
            else:
                print("Invalid choice. Try again.")

        return selVert

    else:
        raise ValueError("Invalid location type. Choose vertex or tile.")
        
def placeSettlement(board, player, init = False, loc = None):
    if loc == None:
        selected = False
        valid = False
        vert = None
        print(f"Choose a settlement location: ")
        while not valid:
            vert = getLocation(board, "vertex")
            if vert.owner == board.empty:
                for r in vert.roads:
                    if ((r[1] == player) | (init)):
                        valid = True
                    else:
                        print("Invalid location. Place settlements next to an owned road.\n")
                        break
                for r in vert.roads:
                    if r[0].owner != board.empty:
                        valid = False
                        print("Invalid location. Settlements cannot be next to any other settlements.\n")
                        break
            else:
                print("Invalid location. That settlement is already owned.\n")
    else:
        vert = board.graph[loc[0]][loc[1]]

    vert.owner = player
    player.settles.append(vert)
    player.trades.add(vert.port)
    board.printBoard()
    player.score += 1

    toRefresh = 0
    for r in vert.roads:
        if r[1] != board.empty: toRefresh += 1
    if toRefresh >= 2: board.getLongestRoad()

def upgradeToCity(board, player, loc = None):
    if loc == None:
        valid = False
        for s in player.settles:
            if s.isCity == False:
                valid = True

        if valid == False:
            print("Error; returning to menu")
            return
        else:
            selected = False
            valid = False
            vert = None
            print(f"Choose a settlement to upgrade: ")
            while not valid:
                vert = getLocation(board, "vertex")
                if vert.owner == player:
                    if vert.isCity == False:
                        valid == True
                    else:
                        print("Invalid location. That's already a city.\n")
                else:
                    print("Invalid location. You don't own that settlement.\n")
    else:
        vert = board.graph[loc[0]][loc[1]]

    vert.isCity == True
    board.printBoard()
    player.points += 1


# Road placement; takes adjacent locations as well
def placeRoad(board, player, vertex = None):
    valid = False
    if vertex == None: vertex = Vertex(board.empty)
    if vertex.owner == board.empty:
        print(f"Select origin settlement for road: ")
        while not valid:
            vertex = getLocation(board, "vertex")
            if (vertex.owner == player) | ((vertex.roads[i][1] == player) for i in range(0, len(roads) + 1)):
                if any(r[1] for r in vertex.roads) == board.empty:
                    valid = True
                else:
                    print("Invalid origin. Please pick a settlement with an open road location.")
            else:
                print("Invalid origin. Please pick a settlement or owned road.")
                
    print("Select destination location for road: ")

    chars = []
    rawChars = toArrows(board, vertex)
    #(vertex, arrow) pairs from toArrows
    openTars = []
    for r in vertex.roads:
        if r[1] == board.empty:
            openTars.append(r[0])
    for c in rawChars:
        if c[0] in openTars:
            chars.append(c)
    if chars != []:
        if len(chars) == 3:
            print(f"Select target for road with 1: {chars[0][1]} 2: {chars[1][1]} 3: {chars[2][1]}")
            while True:
                try:
                    m = int(input())
                except:
                    print("Invalid input. Please try again.")
                    continue
                break
            if m <= 3:
                tar = chars[m - 1][0]
        elif len(chars) == 2:
            print(f"Select target location road with 1: {chars[0][1]} 2: {chars[1][1]}")
            while True:
                try:
                    m = int(input())
                except:
                    print("Invalid input. Please try again.")
                    continue
                break
            if m <= 2:
                tar = chars[m - 1][0]
        else:
            print(f"Only 1 option. Selected {chars[0][1]} automatically.")
            tar = chars[0][0]

        vertex.updateRoad(tar, player)
        board.printBoard()
        board.getLongestRoad(player)
    
    else:
        print("No available roads from this location. Pick another location")